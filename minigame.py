import turtle
import random
import math
turtle.setup(500,500)#setup screen size
window = turtle.Screen()#launch
window.bgcolor("black")#background color of application
window.title("mini game")#title of window

pen = turtle.Turtle()#create object pen
pen.color("red")#set color
pen.shape("triangle")#set shape
pen.speed(0)#set speed when appear on screen
pen.penup()#disable draw line by default
pen.setposition(-200,-200)#set default position
pen.down()#draw the line
#loop to draw square
for i in range(4):
    pen.forward(400)
    pen.left(90)
pen.hideturtle()

player = turtle.Turtle()#create object player
player.color("Green")#set color
player.shape("triangle")#set shape
player.speed(0)#set speed when appear on screen
player.penup()#disable draw line
player.left(90)#rotate left 90deg
player.setposition(0,-230)#set default position

bullet = turtle.Turtle()#create object bullet
bullet.color("yellow")#set color
bullet.shape("triangle")#set shape
bullet.speed(0)#speed when appear on screen
bullet.penup()#disable draw line
bullet.shapesize(0.5,0.5)
bullet.left(90)
x = player.xcor()#player position X
y = player.ycor()+10#player position Y
bullet.setposition(x,y)#set position near player
bullet.hideturtle()#hide bullet by default

bulletstate="ready"#var for reload
#move player left
def move_left():
    x = player.xcor()
    x = x - 15
    bullet.setx(x)
    #check if player near the border
    if x>-190:
        player.setx(x)

#move player right
def move_right():
    x = player.xcor()
    x = x + 15
    bullet.setx(x)
    #check if player near the border
    if x<190:
        player.setx(x)

def fire():#shoot
    global bulletstate
    if bulletstate == "ready":
        bullet.showturtle()
        bulletstate='fire'
def isColision(t1,t2):#check if player's or enemy's colission crossover...
        distance = math.sqrt(math.pow(t1.xcor()-t2.xcor(),2)+math.pow(t1.ycor()-t2.ycor(),2))
        if distance < 15:
            return True
        else:
            return False

turtle.listen()
turtle.onkey(move_left,"Left")
turtle.onkey(move_right,"Right")
turtle.onkey(fire,"space")

enemy = turtle.Turtle() #create object enemy
enemy.color("Green") #add color
enemy.shape("circle") #set shape
enemy.speed(90000) #speed when appear on screen
enemy.penup() #disable draw line
enemy.setposition(0,150) #set default position
enemymove=2 #set movement
#start game
try:
    while True:
        x = enemy.xcor()
        y = enemy.ycor()
        x = x+enemymove
        enemy.setx(x)#move enemy left to right
        if x>190:#if enemy position is near border then move to next line and go left
            enemymove = enemymove * -1
            y=y-30
            enemy.sety(y)
        if x<-190:#if enemy position is near border then move to next line and go right
            enemymove = enemymove * -1
            y=y-30
            enemy.sety(y)
        if bulletstate == 'fire':#if bullet is reloaded than fire
            y = bullet.ycor()
            y = y+20
            bullet.sety(y)
            if y > 190:
                bullet.hideturtle()
                bulletstate='ready'#reload bullet
                bullet.setposition(player.xcor(),player.ycor())#set bullet position in front of player
        if isColision(enemy,bullet):#check if enemy was dead
            enemy.setposition(random.randint(-100,100),random.randint(-100,200))
        if isColision(enemy,player):#check if player was dead
            enemy.hideturtle()
            player.hideturtle()
            print("game over")
            break

    turtle.done()
except Exception as e:
    print("Unexpected close")
