# minigame written on python

## Requirements
<ul>
<li>Python 2.7+</li>
<li>tkinter</li>
</ul>

## Usage
<ul>
<li>Install Python</li>
<li>write command `python minigame.py`</li>
<li>control player with left and right arrow</li>
<li>use space for fire</li>
</ul>


